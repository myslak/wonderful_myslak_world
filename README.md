### Installation
[![pipeline status](https://gitlab.com/myslak/wonderful_myslak_world/badges/master/pipeline.svg)](https://gitlab.com/myslak/wonderful_myslak_world/commits/yml)
[![coverage report](https://gitlab.com/myslak/wonderful_myslak_world/badges/master/coverage.svg)](https://gitlab.com/myslak/wonderful_myslak_world/commits/yml)
1. postgres
2. node.js
    ```
    curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
    sudo apt-get install -y nodejs
    sudo npm install -g @angular/cli

    npm install rxjs@6 rxjs-compat@6 --save
    
    npm install file-saver --save
    npm install @types/file-saver --save
    ```


3. Flask


